\chapter{Die Simulation des Hagen\ndash Poiseuilleschen Flusses}
Zunächst soll mithilfe des SRD-Algorithmus das Problem des Hagen\ndash Poiseuilleschen Flusses in zwei Dimensionen aus der Hydrodynamik gelöst werden.
Da dieses Problem auch analytisch lösbar ist, kann so die Implementierung auf Richtigkeit überprüft werden.\\
Beim Hagen\ndash Poiseuilleschen Fluss handelt es sich um eine stationäre Strömung durch ein Rohr in zwei Dimensionen mit der Länge $L$, das durch zwei Wände im Abstand~$h$ zueinander begrenzt wird.
Das Koordinatensystem wird dabei, wie in Abbildung \ref{quiver} dargestellt, so gewählt, dass die $x$-Achse in Strömungsrichtung zeigt und die $y$-Achse quer zur Flüssigkeitsschicht liegt.
Der Nullpunkt des Koordinatensystems liegt an der linken, unteren Ecke der Röhre.
%Beschreibung des Problems!!

\section{Analytische Herleitung des Geschwindigkeitsfelds}
Das Geschwindigkeitsfeld für den Hagen\ndash Poiseuilleschen Fluss lässt sich, wie in dem Lehrbuch von L. D. Landau und E. M. Lifschitz \cite{Landau} ausführlich erklärt, analytisch über die Navier\ndash Stokes\ndash Gleichung herleiten.
Diese lautet für eine inkompressible, zähe Flüssigkeit mit der Viskosität $\eta$ und der Dichte $\rho$
\begin{equation}
\frac{\uppartial \vec{v}}{\uppartial t} + (\vec{v}\nabla) \vec{v} = \frac{1}{\rho}\nabla p + \frac{\eta}{\rho} \Laplace \vec{v}\:.
\label{navier-stokes}
\end{equation}
Das Geschwindigkeitsfeld ist dabei durch die Variable $\vec{v}$ und der Druck durch $p$ gegeben.

Da es sich bei dem Hagen\ndash Poiseuilleschen Fluss um eine stationäre Strömung in $x$-Richtung handelt, fällt die zeitliche Ableitung weg und die beiden Größen $\vec{v}$ und $p$ hängen nur von $y$ ab.
Durch Reduzierung des Problems auf zwei Dimensionen vereinfacht sich zusätzlich die Navier-Stokes-Gleichung für dieses Problem auf die beiden partiellen Differenzialgleichungen (pDGln)
\begin{equation}
\frac{\uppartial^2 v_x}{\uppartial y^2} = \frac{1}{\eta} \frac{\uppartial p}{\uppartial x}\:, \qquad \frac{\uppartial p}{\uppartial y} = 0\: .
\label{nsg}
\end{equation}

Die Randbedingungen an den beiden Wänden zu diesen pDGln, die auch hier als no-slip-Randbedingungen gewählt werden, lauten
\begin{equation}
v_x(y = 0) = 0 \quad \text{ und } \quad v_x(y = h) = 0 \:. 
\end{equation}

Die zweite Gleichung von (\ref{nsg}) besagt, dass der Druck unabhängig von $y$ und somit zwischen den beiden Wänden konstant ist.
Die erste Gleichung aus (\ref{nsg}) ist auf der rechten Seite nur von $x$ abhängig, während die linke Seite nur von $y$ abhängig ist.
Daraus lässt sich schließen, dass auf beiden Seiten konstante Größen stehen müssen, womit der Druckgradient $\frac{\uppartial p}{\uppartial x}$ konstant ist.

Die pDGln lässt sich nun durch zweimaliges Integrieren lösen:
\begin{equation}
v_x(y) = \frac{1}{2 \eta} \frac{\uppartial p}{\uppartial x} y^2 + ay + b \:.
\label{parabel}
\end{equation}
Aus den beiden Randbedingungen lassen sich die beiden Integrationskonstanten bestimmen. Das Ergebnis ist
\begin{equation}
v_x(y) = - \frac{1}{2\eta} \frac{\uppartial p}{\uppartial x} y (y - h) \:.
\label{eq:hagen}
\end{equation}

Für das Geschwindigkeitsfeld stellt sich somit ein Fluss mit einem parabolischen Geschwindigkeitsprofil in $x$-Richtung ein.
%Das Maximum dieses Geschwindigkeitsprofils ist dabei in der Mitte der Röhre bei $y = h / 2$ erreicht und beträgt
%\begin{equation}
%v_{x,\text{max}} = v_x \! \left(\frac{h}{2}\right) = -\frac{1}{8\eta}\frac{\uppartial p}{\uppartial x} h \:.
%\end{equation}

\section{Geschwindigkeitsfeld aus der Simulation}
\subsection{Die Implementierung}
\label{hagen}
Für die Simulation des Hagen\ndash Poiseuilleschen Flusses werden die Abmessungen der Röhre als $L = \num{100} \, a$ und $h = \num{30} \, a$ gewählt.
Die Masse $m_i$ eines Teilchen sowie Gitterkonstante $a$ der Kollisionszellen betragen wie auch in allen weiteren Simulationen~$\num{1}$.
Für den Zeitschritt wird hier $\updelta t = \num{1}$ gesetzt.
Die drei Größen $a$, $m_i$ und $\updelta t$ bilden in allen Simulationen die charakteristischen Einheiten, durch die die Einheiten aller anderen Größen definiert werden können.
Somit können die Einheiten aller eingestellten Parameter und berechneten Werte durch entsprechende Multiplizieren mit diesen Größen bestimmt werden.
Kenntlich gemacht wird dies durch ein Produkt aus den drei Größen hinter dem Zahlenwert.
Um einen stationären Strom zu simulieren, werden in $y$-Richtung die in Kapitel \ref{ghost} beschriebenen no-slip-Rand\-beding\-ungen und in $x$-Richtung periodische Randbedingungen eingeführt, die sich über
\begin{equation}
x \rightarrow x - L \,\left\lfloor \frac{x}{L} \right\rfloor
\end{equation}
berechnen lassen.
Die Röhre wird dann mit insgesamt $\num{30000}$ Teilchen gefüllt, was einer Teilchenzahl pro Zelle von $M = \num{10}$ entspricht.
Die Geschwindigkeiten der Teilchen zum Startpunkt der Simulation erfüllen eine Maxwell\ndash Boltzmann\ndash Geschwindig\-keits\-ver\-teil\-ung mit $k_{\text{B}} T = \num{0,01} \, m_i a^2 / \updelta t^2$, während der Ort der Teilchen zufällig innerhalb der Röhre ausgewählt wird.
Die hier beschriebenen Abmessungen und Startwerte werden bei allen weiteren Simulationen mit der zweidimensionalen Röhre als Geometrie verwendet.

Der Fluss wird durch eine Kraft $\vec{F} = \num{0,01} \, m_i a / \updelta t^2 \, \vec{e}_x$, die auf jedes Teilchen in jedem Zeitschritt vor dem Kollisionsschritt in $x$-Richtung wirkt, erzeugt.
Der entsprechende Beschleunigungsschritt nach dem Euler-Verfahren sieht dabei wie folgt aus:
\begin{equation}
\vec{v}_i \rightarrow \vec{v}_i + \frac{\vec{F}}{m_i} \updelta t \:.
\end{equation}
Für den Druckgradienten ergibt sich somit
\begin{equation}
\frac{\uppartial p}{\uppartial x} = M \, F = \num{0,1} \, \frac{m_i a}{\updelta t^2}\:.
\end{equation}
Zur Äquilibrierung des Systems wird der Algorithmus vor dem Aufnehmen der Messwerte für das Geschwindigkeitsfeld zunächst über $\num{1000}$ Zeitschritte ausgeführt, um danach die mittlere Geschwindigkeit $\vec{v}_{c,\text{cm}}$, über 2000 Zeitschritte gemittelt, aufzunehmen.
Die zeitliche Mitteilung dient dazu, hydrodynamische Fluktuationen herauszumitteln.

\subsection{Ergebnis}
In Abbildung \ref{quiver} ist das Geschwindigkeitsfeld der Röhre in zwei Dimensionen dargestellt.
Es ist deutlich zu erkennen, dass sich eine stationäre Strömung einstellt, bei der die Geschwindigkeit in der Mitte aufgrund der dissipativen Randbedingungen höher ist als am Rand.
Wie zu erwarten nimmt die Maximalgeschwindigkeit des Flusses mit der Kraft $\vec{F}$ zu.
Ab einem Kraft-Zeitschritt-Produkt von ungefähr $F\, \updelta t \gtrsim \num{0,1} \, m_i a / \updelta t$ ist der Algorithmus jedoch nicht mehr in der Lage, die hohe Kraft und somit die hohe Geschwindigkeitsveränderung pro Zeitschritt zu verarbeiten, sodass bei dieser Para\-meterwahl kein Hagen\ndash Poiseuillescher Fluss mehr zu beobachten ist.

In Abbildung \ref{u} sind dagegen die Messwerte des Geschwindigkeitsprofils $v_{c,\mathrm{cm}, x}(y)$ zu sehen, die wie erwartet eine parabolische Form zeigen.
Diese lassen sich nun mithilfe einer quadratischen Gleichung der Form aus (\ref{parabel}) fitten, sodass sich für das Geschwindigkeitsprofil
\begin{equation}
v_{c, \text{cm}, x} (y) = \underbrace{\num{-0.01}}_{\frac{MF}{2\eta}} \frac{1}{a\, \updelta t} y^2 + \num{0.29} \, \frac{1}{a \, \updelta t}  \, y - \num{0.37}\, \frac{a}{\updelta t}
\end{equation}
ergibt.
Da die Fehler der gefitteten Parameter verschwindend gering sind, werden diese vernachlässigt.
Aus dem ersten Parameter lässt sich zudem noch die Viskosität der Flüssigkeit durch Vergleichen mit Gleichung (\ref{parabel}) bestimmen:
\begin{equation}
\eta = \num{5.08}\, \frac{m_i}{a \, \updelta t}\:.
\end{equation}
Eine mögliche Ursache für die leichten Abweichungen der Messwerte von der gefitteten Funktion, die nicht weiter tragisch sind, könnten die verwendeten Randbedingungen sein. 

\begin{figure}[h]
\centering
\includegraphics[scale=1]{Abbildungen/quiver.pdf}
\caption[Geschwindigkeitsfeld $\vec{v}_{c,\mathrm{cm}}(x, \, y)$ zum Hagen\ndash Poiseuilleschen Fluss bei einer Kraft $\vec{F} = \num{0.01}\, m_i a / \updelta t^2 \, \vec{e}_x$]{Geschwindigkeitsfeld $\vec{v}_{c,\mathrm{cm}}(x, y)$ zum Hagen\ndash Poiseuilleschen Fluss bei einer Kraft $\vec{F} = \num{0.01}\, m_i a / \updelta t^2 \, \vec{e}_x$ simuliert mit dem SRD-Algorithmus. 
Die Pfeile geben dabei die Richtung des Geschwindigkeitsfeldes an. Farblich markiert ist der Betrag $v_{c,\mathrm{cm}}$.}
\label{quiver}
\includegraphics[scale=1]{Abbildungen/u.pdf}
\caption[Geschwindigkeitsprofil $v_{c, \mathrm{cm}, x}(y)$ bei einer Kraft $\vec{F} = \num{0.01}\, m_i a / \updelta t^2 \, \vec{e}_x$ in der Mitte der Röhre bei $x = 50 \, a$]{Geschwindigkeitsprofil $v_{c, \mathrm{cm}, x}(y)$ bei einer Kraft $\vec{F} = \num{0.01}\, m_i a / \updelta t^2 \, \vec{e}_x$ in der Mitte der Röhre bei $x = 50\, a$ mit der analytischen Lösung aus Gleichung (\ref{parabel}) verglichen. Die grüne Linie ist dabei ein quadratischer Fit zu den blauen Messwerten.}
\label{u}
\end{figure}