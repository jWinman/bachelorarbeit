\chapter{Die Simulationsmethode: MPCD}
Für die Simulation des Fluids wird der Raum in Gitterzellen mit dem Gitterabstand~$a$ eingeteilt und die Zeit durch Zeitschritte der Länge $\updelta t$ diskretisiert.
Die Punktteilchen, deren mittlere Teilchenzahl $M$ in dieser teilchenbasierten Simulationsmethode pro Gitterzelle zwischen $3$ und $17$ variieren kann, besitzen dabei eine Masse $m_i$ und kontinuierliche Orts- und Geschwindigkeitsvariablen $\vec{r}_i$ und $\vec{v}_i$.
Die Implementierungen der Simulationen bestehen im Grunde alle aus zwei aufeinanderfolgenden Berechnungen pro Zeitschritt, dem Streaming-Step und einem Kollisionsschritt, wobei der Kollisionsschritt mit verschiedenen Varianten implementiert werden kann.
Hier wird die Stochastic-Rotation Dynamics (SRD) verwendet.
Der Kollisionsschritt wirkt dabei auf alle Teilchen in einer Gitterzelle in der gleichen Art und Weise.

\section{Der Streaming-Schritt}
\label{streaming}
Der Streaming-Schritt sorgt dafür, dass der Ort der Teilchen in jedem Zeitschritt neu berechnet wird.
Der Ort $\vec{r}_i(t + \updelta t)$ für den nächsten Zeitschritt ergibt sich dabei einfach aus der Lösung der ballistischen Bewegungsgleichung des $i$-ten Teilchens mit dem Euler-Verfahren:
\begin{equation}
\vec{r}_i (t + \updelta t) = \vec{r}_i (t) + \updelta t \, \vec{v}_i(t)\:.
\end{equation}

An diesem Schritt ist bereits zu erkennen, dass bei geringen Geschwindigkeiten $\vec{v}_i$ die Teilchen sich für längere Zeit in derselben Gitterzelle aufhalten, wo sie dann in jedem Zeitschritt mit denselben Teilchen wechselwirken.
Teilchen in einer Gitterzelle sind so untereinander nicht mehr unkorreliert.
Dies führt dazu, dass der Algorithmus keine Galilei-Invarianz mehr aufweist, da die Teilchen durch ein periodisches Pseudo-Potential in ihrer Gitterzelle gehalten werden.
Wiederherstellen lässt sich die Galilei-Invarianz, indem, wie in \cite{gompper-review} vorgeschlagen, das Gitter vor jedem Zeitschritt immer um einen zufälligen Vektor im Intervall $[-a / 2, \: a / 2)^d$ mit der Dimension $d$ verschoben wird.
Nach Ausführen des Algorithmus pro Zeitschritt wird dann diese Verschiebung wieder rückgängig gemacht.
Äquivalent zu diesem Vorgehen ist das Verschieben der Teilchen vor jedem Zeitschritt um den zufälligen Vektor, wie es auch in \cite{gompper-review} durchgeführt wird.
Hierfür ist es notwendig, dass das Gitter über die eigentliche Geometrie hinausgeht. 
Wie schon erwähnt, ist diese Verschiebung nur bei kleinen Geschwindigkeiten notwendig. 
Dies ist der Fall, wenn die mittlere freie Weglänge kleiner als die Gitterkonstante ist
\begin{equation}
\lambda = \updelta t \, \sqrt{\frac{k_{\mathrm{B}} T}{m}} < a \:.
\end{equation}
Bei $k_{\mathrm{B}} T$ handelt es sich hierbei um die zu Anfang der Simulation eingestellte kinetische Energie der Teilchen mit der Temperatur $T$ und dem Boltzmann-Faktor $k_{\mathrm{B}}$.

\section{Der Kollisionsschritt: SRD}
\label{SRD}
Im Kollisionschritt der SRD wird die Geschwindigkeit jedes einzelnen Teilchens $i$ in jedem Zeitschritt in Abhängigkeit der mittleren Geschwindigkeit $\vec{v}_{c,\mathrm{cm}}$ aller Teilchen in einer Kollisionszelle $c$ berechnet über
\begin{equation}
\vec{v}_i (t + \updelta t) = \vec{v}_{c,\mathrm{cm}} (t) + \vec{R}_{c} \, \updelta \vec{v}_i (t) \:, \quad \updelta \vec{v}_i(t) = \vec{v}_i(t) - \vec{v}_{c,\mathrm{cm}}(t) \:. \label{deltav}
\end{equation}
Die mittlere Geschwindigkeit für eine Zelle berechnet sich durch
\begin{equation}
\vec{v}_{c,\mathrm{cm}}(t) = \frac{1}{N_{c}} \sum_{j \in c} \vec{v}_j(t) = \frac{1}{M_{c}} \sum_{j \in c} m_j \vec{v}_j (t) \quad \text{mit } M_{c} = \sum_{j \in c} m_j\:.
\end{equation}
Die Anzahl ist dabei $N_c$ und $M_{c}$ die Masse der Teilchen in der Zelle $c$.
Nach (\ref{deltav}) ist $\updelta \vec{v}_i(t)$ die Relativgeschwindigkeit des $i$-ten Teilchens bezüglich der mittleren Geschwindigkeit $\vec{v}_{c,\mathrm{cm}}(t)$.
Diese wird bei der Kollision über die Rotationsmatrix $\vec{R}_{c}$ durch eine zufällige, stochastische Rotation gedreht, die zu jedem Zeitschritt und jeder Kollisionszelle unabhängig voneinander gewählt wird.
Für den zweidimensionalen Fall wird dabei um den Winkel $\alpha = \pm \uppi / 2$ gedreht und das Vorzeichen zufällig mit einer Wahrscheinlichkeit von $\SI{50}{\percent}$ gewählt.
Die SRD stellt somit eine Beziehung zwischen der makroskopischen Geschwindigkeit, die über $\vec{v}_{c,\mathrm{cm}}$ gegeben ist, und der Geschwindigkeit jedes einzelnen Flüssigkeitselements her.

Es lässt sich leicht zeigen, dass die SRD die Erhaltungsgrößen von Masse, Impuls und Energie erfüllt.
Die Massenerhaltung ist erfüllt, da kein Teilchen und somit auch keine Masse während der Simulation verloren geht.
Wie in \cite{vortrag} gezeigt, gilt für den Gesamtimpuls zur Zeit $t + \updelta t$
\begin{subequations}
\begin{align}
\vec{P}(t + \updelta t) &= \sum_{i = 1}^{N} m_i \vec{v}_i(t + \updelta t) = \sum_{c} \sum _{j \in c} m_j \vec{v}_j (t + \updelta t) \\
&= \sum_{c} \sum_{j \in c} \left(m_j \vec{v}_{c,\mathrm{cm}} (t) + \vec{R}_{c} m_j \updelta \vec{v}_j(t)\right) \\
&= \sum_{c} M_{c} \vec{v}_{c,\mathrm{cm}}(t) + \sum_{c} \vec{R}_{c} \underbrace{\sum_{j \in c} m_j \updelta \vec{v}_j (t)}_{ = \text{ } 0}\\
&= \sum_{c} M_{c} \vec{v}_{c,\mathrm{cm}}(t) = \vec{P}(t)\:.
\end{align}
\end{subequations}
Somit ist die Impulserhaltung bei diesem Algorithmus gegeben.
Analog dazu lässt sich auch die Energieerhaltung unter Ausnutzung der Unitärität von $\vec{R}_c$ zeigen, wenn kein äußeres Potential mit in die Simulation einfließt.
All diese Erhaltungsgrößen sind Grundvoraussetzung dafür, dass der Algorithmus auf makroskopischer Ebene die linearisierte Navier\ndash Stokes\ndash Gleichung (vgl. auch (\ref{navier-stokes})), die Newtonsche Fluide mit einer Viskosität $\eta$ beschreibt, erfüllt.
Daher lassen sich mit diesem Algorithmus Probleme aus der Hydrodynamik simulieren.

Weitere Erhaltungsgrößen wie z.B. die Drehimpulserhaltung lassen sich bei Bedarf durch Anpassen des Kollisionschrittes sicherstellen.
%In \cite{gompper-review} wurde der Algorithmus z.B. so erweitert, dass auch der Gesamtdrehimpuls erhalten blieb. Allerdings verlangsamt sich dadurch der Algorithmus um einen Faktor zwei, da der Rotationswinkel $\alpha$ von der Position der Teilchen in der Zelle abhängig gemacht wird.

Zudem wurde von Malevanets und Kapral gezeigt, dass für den SRD ein $H$\ndash Theorem existiert und die Geschwindigkeiten $\vec{v}_i$ im Gleichgewichtszustand einer Max\-well\ndash Boltz\-mann\ndash Ver\-teilung folgen, was sich mithilfe der Simulation auch bestätigen lässt \cite{Kapral}.
%Startet man in der Simulation mit einer Maxwell-Boltzmann-Geschwindig\-keits\-verteilung für die Teilchen, so bleibt diese erhalten.
%Startet man dagegen mit einer gleichverteilten Geschwindigkeitsverteilung, so stellt sich schon nach wenigen tausend Zeitschritten eine Maxwell-Boltzmann-Geschwindig\-keits\-verteilung mit dem Mittelwert von Null ein.

\section{Die Randbedingungen: Ghost-Particles}
\label{ghost}
Als Randbedingungen zwischen den Wänden und der Flüssigkeit werden in den Simulationen im Rahmen dieser Bachelorarbeit nur sogenannte no-slip- oder stick-Rand\-beding\-ungen verwendet.

Bei stick-Randbedingungen verschwindet im Gegensatz zu slip-Randbedingungen nicht nur die Normalkomponente der Geschwindigkeit an der Wand, sondern auch die Tangentialkomponente relativ zur Wand.
Durch stick-Rand\-beding\-ungen lassen sich somit Reibungseffekte zwischen der Wand und der Flüssigkeit beschreiben.
Implementiert werden können solche Randbedingungen durch Invertieren der Geschwindigkeit eines Teilchens $i$ sobald dieses auf die Wand trifft:
\begin{equation}
\vec{v}_i \rightarrow -\vec{v}_i \:.
\end{equation}
Dann wird es durch einen Streaming-Schritt wieder zurück in die Flüssigkeit gesetzt.

Besitzen die Wände komplizierte Geometrien oder wird die in Kapitel \ref{streaming} beschriebene Verschiebung mit implementiert, so kommt es in den Kollisionszellen am Rand zu Teilchenzahlen, die unterhalb der mittleren Teilchenzahl $M$ liegen.
Dies kann durch die Bulk Filling Rule (BFR), wie sie in \cite{boundary-cond} genannt und in \cite{gompper-review} ausführlich beschrieben wird, verbessert werden.
Hierfür werden die teilweise gefüllten Kollisionszellen mit sogenannten Ghost-Particles soweit aufgefüllt, bis die Teilchenzahl der mittleren Teilchenzahl entspricht.
Die Geschwindigkeiten dieser Ghost-Particles genügen dabei einer Maxwell\ndash Boltzmann\ndash Verteilung mit dem Mittelwert von Null und derselben Temperatur $T$ wie den anderen Teilchen im Fluid.
Wird nun ausgenutzt, dass nach dem zentralen Grenzwertsatz die Summe von gaußverteilten Zufallszahlen wieder eine gaußverteilte Zufallszahl ist, so lässt sich die mittlere Geschwindigkeit der mit der Anzahl $N_c$ halbgefüllten Kollisionszelle am Rand schreiben als
\begin{equation}
\vec{v}_{c,\mathrm{cm}} = \frac{ \vec{a} + \sum_{i = 1}^N \vec{v}_i}{M} \: , \quad \sigma^2 = (M - N_c) k_{\mathrm{B}} T \:.
\end{equation}
Die Komponenten des Vektors $\vec{a}$ sind hierbei gaußverteilte Zufallszahlen mit der Varianz~$\sigma$ und dem Mittelwert von Null.
Diese Verbesserung der Randbedingung erhöht die effektive Viskosität des Fluids und sorgt somit für eine bessere Annäherung an die Realität. 
Zudem lässt sich die BFR auch als Thermostat verwenden, da hiermit die Temperatur der Ghost-Particles gesteuert werden kann.