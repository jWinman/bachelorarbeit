\chapter{Einleitung}
Die Simulationsmethode Multi Particle Collision Dynamics (MPCD) ist eine teilchenbasierte Methode zur Simulation von komplexen Flüssigkeiten auf mesoskopischen Längenskalen. 
Die Teilchen in dieser Simulation beschreiben jedoch nicht die Atome oder Moleküle der Flüssigkeit selbst.
Vielmehr wird das Fluid wie bei der Lagrangeschen Darstellung \cite{Greiner} der Hydrodynamik in Flüssigkeitselemente eingeteilt, die Teilcheneigenschaften wie z.B. eine Trajektorie besitzen und somit durch die Teilchen aus der Simulation repräsentiert werden können. 
Zusätzlich werden Wechselwirkungen zwischen den einzelnen Teilchen mitberücksichtigt, die Erhaltungssätze erfüllen, wie z.B. Massen-{}, Impuls- und Energieerhaltung.
Durch Mittelung über die Geschwindigkeiten mehrerer Teilchen, die sich alle örtlich nah beieinander befinden, ergibt sich dann ein Geschwindigkeitsfeld, das äquivalent zu dem aus der Eulerschen Darstellung \cite{Greiner} ist.

Eine erste Simulation mit dieser Methode wurde 1999 von Malevanets und Kapral veröffentlicht \cite{Kapral}.
Diese Methode zeichnet sich vor allem durch ihre kurze Lauf- bzw. Rechenzeit und einfache Implementierung aus.
Zudem ist sie im Gegensatz zu ähnlichen Methoden, die auf der Integration der Langevin-Gleichung aufbauen, in der Lage, Informationen über das gesamte Geschwindigkeitsfeld der Flüssigkeit zu liefern und hydrodynamische Effekte zu berücksichtigen.

Simuliert werden können damit passive Fluide wie z.B. Polymer-Lösungen oder kolloidale Suspensionen oder auch aktive Fluide, die sich z.B. aus chemisch reaktiven Teilchen oder schwimmenden Mikroorganismen zusammensetzen.
Letztere sind dabei von großem Interesse, da diese eine wichtige Rolle in biologischen und chemischen Systemen spielen.
So lassen sich bei biologischen Schwimmern wie Bakterien der Art Bazillus subtilis in einer Petrischale unter einem Mikroskop hydrodynamische Effekte in Form von Turbulenzen beobachten \cite{soft-matter} (vgl. Abb. \ref{Turbulenzen}).
Einfachere experimentelle Realisationen solcher aktiven Fluide sind durch synthetische Schwimmer gegeben, deren Antrieb auf asymmetrischen, chemischen Reaktionen beruhen.
Untersuchungen zu solchen Systemen wurden von S. J. Ebbens und J. R. Howse in \cite{soft-matter-review} zusammengefasst.

\begin{figure}
\subfigure[Angetriebenes, bimetallisches Stäbchen aus Platin und Gold in einer \ce{H_2O_2}-Lösung. Die Ursache des Antriebes ist hier die Elektrophorese \cite{soft-matter-review}.\label{Turbulenzen}]
{
\includegraphics[scale=0.3]{Abbildungen/elektrophorese}
}
\subfigure[Bakterielle Turbulenzen der Art Bazillus subtilis beobachtet von der Unterseite einer Petrieschale. Die weiße Linie ist dabei die Kontaktlinie zwischen Luft, Wasser und Plastik \cite{soft-matter}. \label{Bimetall}]
{
\includegraphics[scale=0.5]{Abbildungen/Turbulenzen_bakterien}
}
\caption{Zwei verschiedene Realisierungen von aktiven Teilchen.}
\end{figure}
Ein Beispiel für einen solchen synthetischen Schwimmer sind zum einen Janus-Teilchen (benannt nach dem römischen, doppelgesichtigen Gott).
Diese bestehen aus einseitig mit Platin beschichtete Polystyren-Teilchen.
In einer \ce{H_2O_2}-Lösung dient das Platin als ein Katalysator, der die chemische Reaktion 
\ce{2H_2O_2 -> 2H_2O + O_2} initiiert.
Der entstehende Sauerstoff sorgt dabei für eine Impulsänderung in eine bestimmte Richtung und somit für den entsprechenden Antrieb des Teilchens.
Eine weitere Realisation sind bimetallische Stäbchen wie sie in Abbildung \ref{Bimetall} abgebildet sind.
Solche Stäbchen bestehen meistens aus Platin und Gold, die sich wiederum in einer \ce{H_2O_2}-Lösung befinden. 
Die Ursache des Antriebs liegt hierbei in der Elektrophorese.
Das Platin-Ende des Stäbchens dient dabei als Anode, an der das \ce{H_2O_2} zu Sauerstoff oxidiert.
Dieser sorgt wie schon beim Janus-Teilchen für den Antrieb.
Aufgrund der Stäbchenform unterscheidet sich jedoch deren Bewegung leicht von der der Janus-Teilchen.
Die Untersuchungen solcher Systeme können letztendlich wichtige Voraussagen für die Medizin liefern.

Mithilfe der MPCD-Methode soll nun in dieser Bachelorarbeit versucht werden, solche hydrodynamische Effekte aktiver Teilchen zu simulieren.
Dazu wird zunächst in Kapitel 2 die Simulationsmethode selbst vorgestellt.
Im darauf\-folgenden Kapitel wird die selbstgeschriebene Implementierung dann anhand des analytisch lösbaren Hagen\ndash Poiseuilleschen Flusses getestet und dessen Viskosität bestimmt.
Im eigentlichen Hauptteil dieser Arbeit wird der Algorithmus dahin umgeändert, dass aus den bisher passiven Flüssigkeitsteilchen aktive werden.
Mit dieser Änderung werden dann drei Themengebiete untersucht: zum einen wird die diffusive Bewegung der aktiven Flüssigkeitsteilchen untersucht. 
Außerdem wird die Vortizität, also die Wirbelstärke der Flüssigkeit genauer untersucht und zum Schluss wird auf die Teilchenzahlverteilung innerhalb eines abgeschlossenen Systems eingegangen.