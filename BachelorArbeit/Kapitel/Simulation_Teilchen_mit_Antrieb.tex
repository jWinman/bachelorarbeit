\chapter{Simulation aktiver Teilchen mit SRD}
In diesem Kapitel wird nun die implementierte MPCD-Simulation, wie sie zuvor beschrieben wurde, so abgeändert, dass das $i$-te Teilchen neben dem Kollisions- und Streaming-Schritt in jedem Zeitschritt durch einen eigenen Antrieb $\vec{\alpha}_i$ in eine beliebige Richtung beschleunigt wird.
Dadurch werden die zuvor passiven Fluidteilchen aktiviert und es können möglicherweise neue hydrodynamische Effekte untersucht werden.

Dazu wird im Algorithmus in jedem Zeitschritt die Geschwindigkeit $\vec{v}_i$ der Teilchen neben dem Kollisionsschritt zusätzlich durch
\begin{equation}
\vec{v}_i \rightarrow \vec{v}_i + \frac{\vec{\alpha}_i}{m_i} \updelta t
\end{equation}
verändert.
Der Antrieb kann dabei in zwei Dimensionen durch einen Winkel $\theta_i(t)$ zur $x$-Achse und der Stärke $\alpha$ des Antriebs beschrieben werden
\begin{equation}
\vec{\alpha}_i(t) = \alpha \pvector{\cos\theta_i(t) \\ \sin\theta_i(t)} \qquad \text{mit} \qquad \theta_i(t) = \theta_i(t - \updelta t) + \theta_{\updelta t} \:.
\label{antrieb}
\end{equation}

Der Winkel $\theta_i(t)$ setzt sich aus dem Winkel des vorigen Zeitschritts $\theta_i(t - \updelta t)$ und einem gaußverteilten Zufallswinkel $\theta_{\updelta t}$ zusammen, der in jedem Zeitschritt und für jedes Teilchen anders ist.
Die Gaußverteilung wird dabei so gewählt, dass die erste Kumulante verschwindet und die Varianz $\lambda$ frei wählbar ist.
Der Anfangswinkel $\theta_i(0)$ wird in den folgenden drei Untersuchungen von System zu System unterschiedlich gesetzt.

\section{Untersuchung der diffusiven Bewegung}
Für die Untersuchung der diffusiven Bewegung der aktiven Teilchen werden die drei Größen $\Avg{ (\theta(t' + t) - \theta(t'))^2 }$, $\avg{\vec{n}(t' + t) \vec{n}(t') }$ und $\Avg{ (\vec{r}(t' + t) - \vec{r}(t))^2}$ gemessen.
Dazu wird nun zunächst in einem kurzen Einschub das theoretische Verhalten erläutert, um dann in Kapitel \ref{diff-ergebnisse} die Messwerte mit den theoretischen Werten vergleichen zu können.

\subsection{Theoretische Beschreibung der diffusiven Bewegung der aktiven Teilchen}
Wie man sich leicht aus der linken Gleichung von (\ref{antrieb}) überlegen kann, lässt sich für den Winkel $\theta_i(t)$ eine Bewegungsgleichung aufstellen, da dieser einer gaußverteilten Zufallsrotation mit dem Zufallsdrehmoment $\xi_{\theta}$ unterliegt.
Diese beinhaltet eine Rotationsreibung $\mathnormal{\Gamma}_{\theta}$, die proportional zur Varianz $\lambda$ ist:
\begin{equation}
\mathnormal{\Gamma}_{\theta} \dot{\theta}_i (t) = \xi_{\theta}(t)
\label{theta}
\end{equation}

Da der Mittelwert von $\theta_{\updelta t}$ verschwindet, verschwindet auch der Mittelwert $\avg{\xi_{\theta}(t)}~= ~0$. 
Zudem gilt für das zweite Moment $\avg{ \xi_{\theta}(t) \xi_{\theta} (t')} = \updelta (t - t')$ für zwei verschiedene Zeitpunkte $t$ und $t'$. 
Durch Integration und Quadrieren lässt sich zeigen, dass sich für das mittlere Änderungsquadrat des Winkels
\begin{equation}
\Avg{\theta_i(t' + t) - \theta_i(t'))^2} = \frac{1}{\mathnormal{\Gamma}_{\theta}^2} t
\label{MSDWinkel}
\end{equation}
ergibt.
Die Klammern $\avg{\ldots}$ geben hierbei immer den Mittelwert über alle Teilchen in der Simulation an.

Ferner lässt sich damit für den normierten Antrieb $\vec{\alpha}_i / \alpha = \vec{n}_i$
\begin{align}
\avg{ \vec{n}(t' + t) \vec{n}(t') } &= \avg{ \cos(\theta(t' + t) - \theta(t')) } \nonumber\\
&= \exp \!\left(- \frac{t}{2\mathnormal{\Gamma}_{\theta}^2}\right)
\label{n_in_j}
\end{align}
 zeigen.

Nimmt man nun für die Teilchenposition $\vec{r}_i(t)$ eine überdämpfte Bewegungsgleichung an, bei der der Inertialterm vernachlässigt wird, so gilt für das $i$-te Teilchen
\begin{equation}
\mathnormal{\Gamma} \dot{\vec{r}}_i (t) = \vec{\alpha}_i(t) + \vec{\xi}(t)\:.
\label{brown}
\end{equation}
Der Faktor $\mathnormal{\Gamma}$ ist dabei der Reibungskoeffizient der Flüssigkeit und $\vec{\xi}(t)$ ist eine gaußverteilte Zufallskraft, die sich aus dem SRD-Kollisionsschritt ergibt. 
Ihr Mittelwert verschwindet und für die Komponenten $m$ und $n$ des Vektors gilt die Relation \\
$\avg{ \xi_m(t) \xi_n(t')} = \lambda_{\xi} \updelta_{mn} \updelta(t - t')$\:.
Der Mean Square Displacement (MSD) ergibt sich nun analog zu Gleichung (\ref{theta}) durch Integration und Quadrieren
\begin{align}
\Avg{(\vec{r}(t' + t) - \vec{r}(t'))^2} &= \frac{2}{\mathnormal{\Gamma}^2} \!\left(\!\left(2\alpha^2 \mathnormal{\Gamma}_{\theta}^2 + \lambda\right) t - 2\alpha^2 \mathnormal{\Gamma}_{\theta}^2 \!\left(1 - \exp\!\left(- \frac{t}{2\mathnormal{\Gamma}_{\theta}^2}\right)\right)\right) \nonumber\\
&\approx \frac{2}{\mathnormal{\Gamma}^2}\!\left(2\alpha^2 \mathnormal{\Gamma}_{\theta}^2 + \lambda_{\xi}\right) t \:.
\label{r^2}
\end{align}
Die Näherung gilt hierbei für kleine Rotationsreibungen $\mathnormal{\Gamma}_{\theta}$ und somit auch kleine Varianzen $\lambda$.
Durch einen Vergleich von Gleichung (\ref{r^2}) mit der Definition der Diffusionskonstante $D$ in zwei Dimensionen, die $\Avg{(\vec{r}(t' + t) - \vec{r}(t'))^2} = 4Dt$ lautet, ergibt sich für die Diffusionskonstante
\begin{subequations}
\begin{align}
D(\alpha) &= \frac{1}{\mathnormal{\Gamma}_{\theta} \, \mathnormal{\Gamma}} \alpha^2 + \frac{\lambda_{\xi}}{2 \mathnormal{\Gamma}^2} 
\label{eq:Dalpha}\\
&= D_{\mathnormal{\Gamma}} \alpha^2 + D_0 \:.
\end{align}
\end{subequations}
$D_0$ ist die Translationsdiffusionskonstante, die sich auch für Fluide mit passiven Teilchen ergeben würde.

An dieser Stelle sollte jedoch erwähnt werden, dass Gleichung (\ref{brown}) das hier betrachtete Problem sehr vereinfacht und genähert beschreibt.
Vernachlässigt werden in der Gleichung die hydrodynamischen Wechselwirkungen, während diese jedoch in der Simulation eine wichtige Rolle spielen.
In der Simulation wird über den SRD-Kollisionsschritt mit berücksichtigt, dass die Geschwindigkeit des $i$-ten Teilchens auch eine Auswirkung auf die Bewegung eines benachbarten $j$-ten Teilchens haben kann.
Jedoch spiegelt sich das nicht in Gleichung (\ref{brown}) wider.
Die Näherung wird hier jedoch trotz alledem verwendet, um einen groben Vergleich zwischen der theoretischen Überlegung und den Ergebnissen der Simulation zu erhalten.

\subsection{Die Implementierungen}
\label{diff-implementierungen}
\paragraph{Implementierung zur Untersuchung des MSD:}
Um den MSD in der MPCD-Simu\-lat\-ion zu untersuchen, wird die oben beschriebene Implementierung, also die MPCD-Methode mit Antrieb, für zwei verschiedene, zweidimensionale Geometrien durchgeführt.
Zum einen wird ein Kreis mit dem Radius $R$ betrachtet und zum anderen ein Rechteck mit der Höhe $h$ und der Länge $L$.

Die Abmessungen des Rechtecks sind dabei wie schon zuvor beim Hagen\-\ndash Poiseu\-illeschen Fluss $L = 100 \, a$ und $h = 30 \, a$ mit $a = 1$ und die Gesamtteilchenzahl beträgt ebenfalls $\num{30000}$, sodass sich für die mittlere Teilchenzahl pro Zelle $M = \num{10}$ ergibt.
Bei beiden Geometrien wird wieder mit einer Maxwell\ndash Boltzmann\ndash Geschwindig\-keits\-verteil\-ung mit der Temperatur $k_{\mathrm{B}} T = \num{0.01} \, m_i a^2 / \updelta t^2$ gestartet.
Zur besseren Vergleichbarkeit wird der Radius des Kreises so gewählt, dass die mittlere Teilchenzahl pro Zelle ebenfalls $M = 10$ beträgt. 
Die Gesamtteilchenzahl beträgt hierbei wiederum $\num{30000}$ mit der Masse eines Teilchens $m_i = 1$.
Die Anfangsposition der Teilchen wird wieder zufällig innerhalb der Geometrie gesetzt.
Für beide Geometrien wird der Zeitschritt $\updelta t = 1$ gesetzt und die Varianz des gaußverteilten Zufallswinkels $\lambda = \num{0,01}$.
Als Anfangswinkel $\theta(0)$ werden zufällig ausgewählte Werte zwischen $\num{0}$ und $\num{2}\uppi$ verwendet.
Auch diese Parameter werden, wenn nicht anders angegeben, bei allen weiteren Simulationen mit dem Rechteck bzw. Kreis verwendet.

Als Randbedingungen dienen weiter die in Kapitel \ref{ghost} beschriebenen no-slip-Rand\-bed\-ing\-ungen.
Nach einer Äquilibrierung von $\num{5000}$ Zeitschritten werden beide Implementierungen für verschiedene Antriebsstärken $\alpha$ zwischen $\num{0}$ und $\num{0,1} \, m_i a / \updelta t^2$ über eine Zeitdauer von $\num{5000}$ ausgeführt, um so den zeitlichen Verlauf der Größen $\Avg{ (\theta(t' + t) - \theta(t'))^2 }$, $\avg{\vec{n}(t' + t) \vec{n}(t') }$ und $\Avg{ (\vec{r}(t' + t) - \vec{r}(t))^2 }$ zu untersuchen. 
Aus dem MSD lässt sich dann die Diffusionskonstante $D(\alpha)$ bestimmen. 

\paragraph{Implementierung zur Bestimmung des Reibungskoeffizienten $\mathnormal{\Gamma}$:}
\label{Gamma}
In einer weiteren Simulation wird der Reibungskoeffizient $\mathnormal{\Gamma}$ des aktiven Fluids bestimmt, um einen Vergleich des theoretischen Werts für $D(\alpha)$ mit dem aus der Simulation gewonnen Wert zu ermöglichen.

Hierzu wird wieder die zweidimensionale Röhre mit den entsprechenden Randbedingungen und Abmessungen aus dem Kapitel \ref{hagen} verwendet.
Auch der Anfangs\-winkel~$\theta(0)$ wird wieder zufällig aus dem Intervall $[0, \: 2\uppi)$ ausgewählt.
Der Zeit\-schritt~$\updelta t$ beträgt wieder $\num{1}$ und die Varianz des Antriebs beträgt $\lambda = \num{0,01}$.
Eines der $\num{30000}$~Teilchen aus der Simulation erhält dabei anstatt des Antriebs eine konstante Kraft in $x$-Richtung $\vec{F} = \num{0,05}\, m_i a / \updelta t^2 \, \vec{e}_x$ und wird somit nach einer Äquilibrierungsphase von $\num{5000}$ Zeitschritten durch das Fluid gezogen.

Der Algorithmus läuft insgesamt über $\num{50000}$ Zeitschritten und für drei verschiedene Antriebsstärken $\alpha / (m_i a / \updelta t^2) \in \{\num{0}; \: \num{0,005}; \: \num{0,001}\}$, die viel kleiner als der Betrag der Kraft $F$ sind.
Gemessen wird die Geschwindigkeit $\overline{\vec{v}}$ des Teilchens nach $\num{5000}$, $\num{10000}$ und $\num{50000}$ Zeitschritten, über die die Geschwindigkeiten auch gemittelt werden.
Der Reibungskoeffizient $\mathnormal{\Gamma}$ der Stokesschen Reibung lässt sich dann berechnen über
\begin{equation}
\mathnormal{\Gamma} = \frac{F}{\overline{v}} \:.
\label{stokes}
\end{equation}

\subsection{Ergebnisse}
\label{diff-ergebnisse}
\begin{figure}[!ht]
\begin{center}
\centering
\subfigure[Kreisförmige Geometrie. \label{variance_kreis}]
{
\includegraphics[scale=0.66]{Abbildungen/variance_kreis.pdf}
}
\newpage
\subfigure[Rechteckige Geometrie. \label{variance_rectangle}]
{
\includegraphics[scale=0.66]{Abbildungen/variance_rectangle.pdf}
}
\caption[Messwerte für $\Avg{ (\theta(t' + t) - \theta(t'))^2}$, $\avg{\vec{n}(t' + t) \vec{n}(t') }$ und $\Avg{ (\vec{r}(t' + t) - \vec{r}(t))^2 }$]{Messwerte für mittlere Änderungsrate des Winkels~$\Avg{ (\theta(t' + t) - \theta(t'))^2}$, $\avg{\vec{n}(t' + t) \vec{n}(t') }$ und der MSD $\Avg{ (\vec{r}(t' + t) - \vec{r}(t))^2 }$ für verschiedene $\alpha$ und Geometrien aus der Simulation und entsprechende Fits.}
\end{center}
\end{figure}

Direkt aus der Simulation gemessen wurde die mittlere Änderungsrate des Winkels $\Avg{(\theta(t' + t) - \theta(t'))^2 }$, $\avg{\vec{n}(t' + t) \vec{n}(t')}$ und der MSD $\Avg{ (\vec{r}(t' + t) - \vec{r}(t))^2 }$.
Der Zeitpunkt~$t'$ wurde dabei nach einer Äquilibrierungsphase von $\num{5000}$ Zeitschritten gesetzt.
Der zeitliche Verlauf dieser Größen ist in Abbildung \ref{variance_kreis} für die Geometrie des Kreises und für eine rechteckige Geometrie in Abbildung \ref{variance_rectangle} dargestellt.
Während Messwerte bis $\num{5000}$ Zeitschritte aufgenommen wurden, sind hier allerdings nur die ersten $\num{1000}$ Schritte aufgetragen, da in diesem der interessante lineare Bereich für wenige Zeitschritte für den MSD besser zu sehen ist.

Zunächst ist zu sehen, dass die beiden Größen $\Avg{ (\theta(t' + t) - \theta(t'))^2 }$ und $\avg{\vec{n}(t' + t) \vec{n}(t') }$ unabhängig von der Geometrie sind.
Aus einem Fit mit den Gleichungen (\ref{MSDWinkel}) für das mittlere Änderungsquadrat des Winkels und (\ref{n_in_j}) für die Größe $\avg{ \vec{n}(t' + t) \vec{n}(t') }$ über die ersten $\num{1000}$ Messwerte ergeben sich für beide Geometrien die Funktionen
\begin{align}
\Avg{ (\theta(t' + t) - \theta(t'))^2 } &= \num{0,0001} \, \frac{t}{\updelta t} \\
\avg{ \vec{n}(t' + t) \vec{n}(t') } &= \exp\!\left(\num{-0,00005} \, \frac{t}{\updelta t}\right) \:.
\end{align}
Der Rotationsreibungkoeffizient $\mathnormal{\Gamma}_{\theta}$ lässt sich somit berechnen zu
\begin{equation}
\mathnormal{\Gamma}_{\theta} = \num{98,74} \, \frac{m_i}{\updelta t}\:.
\end{equation}
Die statistischen Fehler des Fits sind hierbei so gering, dass sie vernachlässigt werden können.

Für den zeitlichen Verlauf des MSD ist zu erkennen, dass diese Größe für beide Geometrien bis zu ungefähr $\num{500}$ Zeitschritten linear in $t$ ist.
Danach weichen die Kurven für höhere $\alpha$ von der Linearität leicht ab.
Bei dem Rechteck ist dies deutlicher zu sehen als bei den Werten zum Kreis.
Erklären lässt sich diese Abweichung vor allem durch die Abgeschlossenheit der verwendeten Geometrien, die bei der Herleitung des MSD nicht berücksichtigt wurde.
Ebenso zeigt sich, dass der MSD aufgrund der Linearität für $t > \num{0}$ nicht nach oben beschränkt ist.
Dies ist allerdings nicht vereinbar mit der Abgeschlossenheit des Systems.
Beim Kreis z.B. gilt für jedes der Teilchen
\begin{equation}
(\vec{r}_i(t' + t) - \vec{r}_i(t')) < (2 R)^2 \:,
\end{equation}
sodass auch der Mittelwert dieser Größe über alle Teilchen diese Ungleichung erfüllen muss.
Es lässt sich sogar beobachten, dass der MSD für hohe Zeit beim Kreis gerade unabhängig von $\alpha$ gegen $R^2$ geht.

Durch Fits im linearen Bereich des MSD lässt sich die Diffusionskonstante $D$ nach Gleichung (\ref{eq:Dalpha}) für die verschiedenen Antriebsstärken $\alpha$ bestimmen.
Die verwendeten Antriebsstärken für den Kreis sind dabei in Abbildung \ref{variance_kreis} und die für das Rechteck in Abbildung \ref{variance_rectangle} zu finden.
Die gefitteten Werte $D$ wurden dann in Abhängigkeit von $\alpha$ in Abbildung \ref{D_alpha_kreis} und \ref{D_alpha_rectangle} geplottet.
Es zeigt sich für beide Geometrien eine quadratischen Abhängigkeit von $\alpha$, wie es in der Theorie vorhergesagt wurde.
Fittet man die beiden Diffusionskonstanten, so ergibt sich
\begin{subequations}
\begin{align}
D_{\text{Kreis}}(\alpha) &= \num{1,0347} \, \frac{\updelta t^3}{m_i^2} \, \alpha^2 + \num{0,0006} \, \frac{a^2}{\updelta t}
\label{eq:Dkreis} \:,\\
D_{\text{Rechteck}}(\alpha) &= \num{0,8047} \, \frac{\updelta t^3}{m_i^2} \, \alpha^2 + \num{0,0006} \, \frac{a^2}{\updelta t} \:.
\label{eq:DRechteck}
\end{align}
\end{subequations}
Wie man sieht, ergibt sich für beide Fits die gleiche Translationsdiffusionskonstante $D_0 = \num{0,0006} \, a^2 / \updelta t$.
Diese ist sowohl unabhängig vom Antrieb als auch von der Geometrie.
Sie hängt also nur von Eigenschaften der Flüssigkeit selbst ab.
Dagegen ist der Vorfaktor $D_{\mathnormal{\Gamma}}$ sehr wohl von der Geometrie abhängig, obwohl ihr Einfluss in der Theorie vernachlässigt wurde.

Nichtsdestotrotz wurde dieser Vorfaktor durch eine weitere Simulation ermittelt.
Dazu wurde der Reibungskoeffizient $\mathnormal{\Gamma}$, wie in \ref{Gamma} beschrieben, ermittelt, aus dem dann der Vorfaktor nach Gleichung (\ref{eq:Dalpha}) berechnet werden kann.
Für die Geschwindigkeiten nach den drei angegebenen Zeiten ergaben sich für eine Antriebsstärke von $\alpha = \num{0,0005} \, m_i a / \updelta t^2$:
\begin{align}
\left.
\begin{array}{l l}
\overline{v}_{\num{5000}} &= \num{0,183} \, \frac{a}{\updelta t}\\
\overline{v}_{\num{10000}} &= \num{0,187} \, \frac{a}{\updelta t}\\ 
\overline{v}_{\num{50000}} &= \num{0,185} \, \frac{a}{\updelta t}\\
\end{array}\right\rbrace \overline{v} \approx \num{0,19} \, \frac{a}{\updelta t} \:.
\end{align}
Die Geschwindigkeiten der anderen beiden Antriebsstärken wichen vernachlässigbar gering von diesen ab, weshalb sie hier nicht aufgeführt werden.
Über Gleichung (\ref{stokes}) lässt sich nun der Reibungskoeffizient und der Vorfaktor $D_{\mathnormal{\Gamma}}$ berechnen
\begin{equation}
\mathnormal{\Gamma} = \num{0,26} \, \frac{m_i}{\updelta t} \:, \qquad D_{\mathnormal{\Gamma}} = \num{1296} \, \frac{\updelta t^3}{m_i^2}\:.
\end{equation}
Ein Vergleich zeigt, dass dieser Wert für $D_{\mathnormal{\Gamma}}$ weit oberhalb derer aus den Gleichungen (\ref{eq:Dkreis}) und (\ref{eq:DRechteck}) liegt.
Dies liegt zum einen daran, dass die Theorie, wie bereits erwähnt, so nicht unbedingt gültig ist. 
Andererseits war möglicherweise auch die Messmethode zur Bestimmung des Reibungskoeffizienten fehlerhaft, da sich beim Hindurchziehen des Teilchens durch das Fluid die gesamte Flüssigkeit in Bewegung setzt.
Dadurch wurde möglicherweise ein geringerer Reibungskoeffizient und somit ein höheres $D_{\mathnormal{\Gamma}}$ gemessen.

\begin{figure}[h]
\centering
\subfigure[Kreisförmige Geometrie. \label{D_alpha_kreis}]
{
\includegraphics[width=\textwidth]{Abbildungen/D_alpha_kreis.pdf}
}
\hfill
\subfigure[Rechteckige Geometrie. \label{D_alpha_rectangle}]
{
\includegraphics[width=\textwidth]{Abbildungen/D_alpha_rectangle.pdf}
}
\hfill
\caption[Diffusionskonstante $D(\alpha)$ in Abhängigkeit der Antriebsstärke $\alpha$]{Diffusionskonstante $D(\alpha)$ in Abhängigkeit der Antriebsstärke $\alpha$ für verschiedene Geometrien. Die grüne Linie ist dabei ein quadratischer Fit der aus der Simulation gewonnenen Messwerte.}
\end{figure}

\section{Untersuchung der Vortizität}
Um die Vortizität, also die Wirbelstärke, des Fluids aus aktiven Teilchen zu untersuchen, wird dazu numerisch die Rotation des Geschwindigkeitsfeldes bestimmt.
Da die Simulation nur für zwei Dimensionen durchgeführt wurde, ist nur die $z$-Komponente von Interesse.
Abgeleitet wird über den symmetrischen Differenzenquotient mithilfe der Geschwindigkeiten $\vec{v}_{c, \mathrm{cm}}$, die sich nach der zeitlichen Mittelung ergeben:
\begin{align}
\omega_z &= (\nabla \times \vec{v})_z \\
&\approx \frac{1}{a} \!\left( v_{c, \mathrm{cm}, x} \! \left(x, y + \frac{a}{2}\right) - v_{c, \mathrm{cm}, x}\left(x, y - \frac{a}{2}\right) \right.\nonumber\\
&\quad\qquad \! \left.- v_{c, \mathrm{cm}, y}\! \left(x + \frac{a}{2}, y\right) + v_{c,\mathrm{cm}, y}\!\left(x - \frac{a}{2}, y\right)\right)
\label{omega}
\end{align}
Betrachtet wird nun jedoch das Quadrat dieser Größe, $\omega^2 = \omega_z^2$.
Um eine Aussage über Vortizität des gesamten Systems treffen zu können, kann zusätzlich noch über den Ort gemittelt und mit dem mittleren Geschwindigkeitsquadrat $\avg{ v_{c, \mathrm{cm}}^2 }$ normiert werden, sodass letztendlich die Größe $\avg{\omega^2 } / \avg{v_{c, \mathrm{cm}}^2}$ betrachtet wird.

\subsection{Die Implementierungen}
\label{vortizität}
Zur Untersuchung der Vortizität wird wieder die MPCD-Methode mit Antrieb für zwei verschiedene Geometrien implementiert: zum einen die zweidimensionale Röhre und zum anderen das abgeschlossene Rechteck.

Es werden wieder die entsprechenden Randbedingungen, Anfangsbedingungen und Abmessungen implementiert wie sie bereits zuvor für die entsprechenden Geometrien in \ref{hagen} und \ref{diff-implementierungen} verwendet wurden.
Jedoch beträgt der Zeitschritt diesmal $\num{0,1}$, um auch höhere Antriebe problemlos simulieren zu können.
Die Varianz des gaußverteilten Winkels $\theta_{\updelta t}$ wird konstant bei $\num{0,01}$ gehalten.

Für die zweidimensionale Röhre wird der Anfangswinkel $\theta(0)$ für alle Teilchen auf $\num{0}$ gesetzt, sodass sich für kleine Zeiten in der Simulation trivialerweise ein stabiler Hagen\-\ndash Pois\-euillescher Fluss in $x$-Richtung einstellt.
Der Anfangswinkel für das abgeschlossene Rechteck wird wieder zufällig aus dem Intervall $[0,\: 2\uppi)$ gewählt.

Es werden nun für verschiedene Antriebsstärken $\alpha$ Simulationen durchgeführt, in denen der Algorithmus pro Messvorgang $\num{6000}$ Zeitschritte läuft, von denen dann die Messwerte über die letzten $\num{1000}$ Zeitschritte gemittelt aufgenommen werden.
Dieser Messvorgang wird pro Simulation $\num{50}$ mal wiederholt, sodass der Algorithmus letztendlich über $\num{300000}$ Zeitschritte läuft.
Die Antriebsstärken variieren dabei zwischen $\num{0,005}$ und $\num{0,1} \, m_i a / \updelta t^2$ in $\num{0,005}$-er-Schritten.

Gemessen wird zunächst wieder die Geschwindigkeit $\vec{v}_{c, \mathrm{cm}}$, woraus dann nach Gleichung (\ref{omega}) die Vortizität bzw. deren Quadrat berechnet werden kann.
\newpage

\subsection{Ergebnisse}
\paragraph{Das Rechteck:}
\begin{figure}[h!!!!!!!!!]
\centering
\subfigure[Antriebsstärke $\alpha = \num{0.005} \, m_i a / \updelta t^2$. \label{rectangle_0.005}]
{
\includegraphics[width=\textwidth]{Abbildungen/quiver0-005-0-rectangle.pdf}
}
\subfigure[Antriebsstärke $\alpha = \num{0.1} \, m_i a / \updelta t^2$. \label{rectangle_0.1}]
{
\includegraphics[width=\textwidth]{Abbildungen/quiver0-100-0-rectangle.pdf}
}
\hfill
\caption[Das Quadrat der Vortizität $\omega^2$ des Rechtecks für verschiedene Antriebsstärken $\alpha$]{Das Quadrat der Vortizität $\omega^2$ (farblich markiert) des Rechtecks für verschiedene Antriebsstärken $\alpha$ und der Varianz $\lambda = \num{0.01}$ nach $\num{6000}$ Zeitschritten davon über $\num{1000}$ Zeitschritte gemittelt. Das Geschwindigkeitsfeld $\vec{v}_{c, \mathrm{cm}}$ wird durch die Pfeile repräsentiert.}
\label{rectangle}
\end{figure}

\begin{figure}[h!!]
\centering
\includegraphics[width=\textwidth]{Abbildungen/omega_square3d_rectangle.pdf}
\caption[$\Avg{ \omega(\alpha, t)^2 } / \Avg{ v_{c, \mathrm{cm}}(\alpha, t)^2 }$ in Abhängigkeit der Zeit $t$ und der Antriebsstärke $\alpha$ für das Rechteck]{Gemitteltes und mit dem Quadrat der Geschwindigkeit normiertes Quadrat der Vortizität $\Avg{ \omega(\alpha, t)^2 } / \Avg{ v_{c, \mathrm{cm}}(\alpha, t)^2 }$ in Abhängigkeit der Zeit $t$ und der Antriebsstärke $\alpha$ bei der Varianz $\lambda = \num{0.01}$ für die Geometrie des Rechtecks. Die Mittelungen $\avg{\ldots}$ gehen hierbei über alle Orte $x$, $y$.}
\label{omega_square_rectangle}
\end{figure}

In den beiden Abbildungen \ref{rectangle_0.005} und \ref{rectangle_0.1} sind zunächst die Geschwindigkeitsfelder $\vec{v}_{c, \mathrm{cm}}$ für das Rechteck für die stärkste und schwächste gewählte Antriebskraft abgebildet.
Aufgenommen wurden diese Bilder nach dem ersten Messvorgang. 
Das bedeutet, der Algorithmus läuft $\num{6000}$ Zeitschritte, wobei davon über die letzten $\num{1000}$ Zeitschritte gemittelt wird.
Farblich markiert ist hier das Quadrat der Vortizität $\omega^2$.

Anhand der Skala für $\omega^2$ lässt sich direkt sehen, dass die Stärken der Wirbel bei höheren Antrieben viel größer ist als bei kleinen.
Dies liegt natürlich daran, dass die Geschwindigkeiten $\vec{v}_{c, \mathrm{cm}}$, von denen die Vortizität abhängt, mit höherem Antrieb zunehmen.
Allerdings lässt sich auch deutlich erkennen, dass sich bei niedrigen Antrieben mehr Wirbel ergeben als bei hohen.
Zu erkennen ist dies in den beiden Abbildungen an den roten und grünen Punkten, die auf eine hohe Vortizität hindeuten.
Da der Betrag der Vortizität bekanntlich in der Mitte eines Wirbels am höchsten ist, detektieren diese Punkte somit einen Wirbel im Geschwindigkeitsfeld.
Nun erkennt man in Abbildung \ref{rectangle_0.005}, dass es mehr grüne und rote Punkte gibt als in Abbildung \ref{rectangle_0.1}.
In Abbildung~\ref{rectangle_0.1} sind dagegen nur einige wenige Wirbel zu sehen, die sich vor vor allem in der Mitte der Geometrie aufhalten.
Zurückführen lässt sich diese Beobachtung auf die Tatsache, dass ein angetriebenes Teilchen, welches mit höherem Antrieb eine ballistische Bewegung ausführt, seine Richtung langsamer ändert, als ein Teilchen mit geringerem Antrieb.
Da in dieser Simulation die SRD-Kollisionen unter den Teilchen die Richtung~$\theta$ des Antriebs nicht ändert, verhindert diese nicht den zu beobachtenden Effekt.
Somit lässt sich sagen, dass höhere Antriebe für weniger Wirbel sorgen, die dafür aber eine größere Stärke haben, während kleine Antriebe für viele kleine Wirbel sorgen.

Weiterhin wurde nun noch die Mittelung wie in oben beschrieben durchgeführt, um die Größe $\avg{\omega(\alpha, t)^2} / \avg{v_{c, \mathrm{cm}}(\alpha, t)^2}$ zu berechnen.
%Die Normierung mit dem gemittelten Geschwindigkeitsquadrat soll für eine bessere Vergleichbarkeit zwischen den verschiedenen Antriebsstärken sorgen, da so sowohl die Anzahl als auch die Stärke der Wirbel berücksichtigt wird.
Diese wurde dann für die $\num{20}$ verschiedenen Antriebsstärken und $\num{20}$ Messvorgänge in Abbildung \ref{omega_square_rectangle} dargestellt.
Darin ist zu sehen, dass das auf das mittlere Geschwindigkeitsquadrat normierte mittlere Quadrat der Vortizität am größten bei hohen Antriebskräften und langen Zeiten ist.
Niedriger ist die Größe allerdings bei kleinen Zeiten und kleinen Antrieben $\alpha$.
Im zeitlichen Verlauf nimmt diese Größe dann zu, was für höhere Antriebe schneller passiert als für kleine.
So zeigt sich ebenfalls bei großen Antrieben, dass $\avg{\omega(\alpha, t)^2} / \avg{v_{c, \mathrm{cm}}(\alpha, t)^2}$ bei kleinen Zeiten $t$ klein ist, jedoch steigt diese dann mit der Zeit schnell an.
\FloatBarrier

\paragraph{Die zweidimensionale Röhre:}
\begin{figure}[h!!!!!!!!!]
\centering
\subfigure[Von $\num{26000}$ bis $\num{27000}$ Zeitschritten gemittelt. \label{pipe_26}]
{
\includegraphics[width=\textwidth]{Abbildungen/quiver0_100-26-pipe.pdf}
}
\subfigure[Von $\num{31000}$ bis $\num{32000}$ Zeitschritten gemittelt. \label{pipe_31}]
{
\includegraphics[width=\textwidth]{Abbildungen/quiver0_100-31-pipe.pdf}
}
\hfill
\caption[Geschwindigkeitsfelder $\vec{v}_{c, \mathrm{cm}}$ der zweidimensionalen Röhre bei einer Antriebsstärke $\alpha = \num{0.1}$ für versch. Zeiten]{Geschwindigkeitsfelder $\vec{v}_{c, \mathrm{cm}}$ der zweidimensionalen Röhre bei einer Antriebsstärke $\alpha = \num{0.1}$ und Varianz $\lambda = \num{0.01}$ nach verschiedenen Zeitschritten über $\num{1000}$ Zeitschritte gemittelt. Farblich markiert ist der Betrag $v_{c, \mathrm{cm}}$.}
\label{2D-röhre}
\end{figure}

In Abbildung \ref{2D-röhre} finden sich für das Geschwindigkeitsfeld der zweidimensionalen Röhre zwei Plots wieder.
Beide Plots sind bei einer Antriebsstärke von $\alpha = \num{0,1} \, m_i a / \updelta t^2$ aufgenommen worden.
\ref{pipe_26} stellt dabei die gemittelte Geschwindigkeit von Zeitschritt $\num{26000}$ bis $\num{27000}$ dar.
Der andere Plot, \ref{pipe_31}, wurde von Zeitschritt $\num{31000}$ bis $\num{32000}$ gemittelt aufgenommen.

Wie sich direkt erkennen lässt, ist der Hagen\ndash Poiseuillesche Fluss bei beiden gewählten Antriebsstärke nicht stabil.
Dieser zerfällt schon nach ungefähr $\num{10}$ Messvorgängen. 
Auch in einer weiteren Simulation mit den gleichen Parametern aber einem weit höher gewählten Antrieb von $\alpha = \num{100} \, m_i a / \updelta t^2$ zerfällt der Hagen\ndash Poiseuillesche Fluss schon nach unter $\num{15}$ Messvorgängen.
Es liegt daher nahe, dass es keine Antriebsstärke gibt, bei dem der Hagen\ndash Poiseuillesche Fluss stabil bleibt.

Zudem ist zu erkennen, dass sich, nachdem der Hagen\ndash Poiseuillesche Fluss verschwunden ist, auch weiterhin immer wieder Flüsse mit Verwirbelungen sowohl in positiver als auch negativer $x$-Richtung ausbilden.
Für höhere $\alpha$ dreht sich dabei der Fluss schneller um als für kleinere $\alpha$. 

Betrachtet man die Vortizität bzw. deren Quadrat, $\omega_z(t, \alpha)$ bzw. $\omega(t, \alpha)^2$, bei diesem System, so stellt man zunächst fest, dass der Hagen\ndash Poiseuillesche Fluss bei kleinen Zeiten bereits ein Vortizitätsquadrat 
$\omega(t, \alpha)^2 = \!\left(\frac{1}{2\eta}\frac{\uppartial p(t, \alpha)}{\uppartial x}\right)^2 (y - h)^2$
größer als Null besitzt.
%Das Quadrat der Vortizität $\omega(t, \alpha)^2$ geht somit für einen konstanten Ort quadratisch mit dem Druckgradienten, der zumindest beim Starten der Simulation linear zu $\alpha$ ist.
Problematisch ist hierbei, dass das Quadrat der Vortizität $\Avg{\omega(t, \alpha)^2 }$ des Hagen\-\ndash \-Poiseuilleschen Flusses im Mittel größer ist als die des zerfallenen Geschwindigkeitsfeldes.
Zudem nimmt $\Avg{\omega(t, \alpha)^2 }$ bereits für den noch stabilen Hagen-Poiseulleschen Fluss für alle $\alpha$ mit der Zeit ab, bevor der Fluss instabil wird und in Wirbel zerfällt.
%Dieser Übergang passiert für das Mittel der quadratischen Vortizität stetig.
Daher lässt sich mithilfe der gemittelten quadratischen Vortizität nicht der Punkt bestimmen, bei dem der Hagen\ndash Poiseuillesche Fluss instabil wird.
Es kann also festgehalten werden, dass die Vortizität $\omega_z$ kein guter Ordnungsparameter dafür ist, den Übergang zwischen dem in $x$-Richtung translationssymmetrischen Hagen\ndash Poiseuilleschen Fluss und dem verwirbelten Geschwindigkeitsfeld in der zweidimensionale Röhre zu detektieren.

\section{Untersuchung der Teilchenzahldichte}
Wie in \cite{wall} von Gompper und Elgeti gezeigt wird, verhält sich ein angetriebenes Teilchen, das einer Brownschen Dynamik in einem Fluid unterlegen ist, so, dass es in der Nähe von Wänden eine höhere Aufenthaltswahrscheinlichkeit besitzt.
Dies liegt daran, dass sich die Orientierung des Antriebs nur sehr langsam ändert.
Das bedeutet: wird das Teilchen nun einmal in die Richtung der Wand angetrieben, so behält es für längere Zeit diese Richtung bei.
%Dadurch kollidiert es zwar mit der Wand, aber aufgrund des Antriebes wird es immer wieder gegen die Wand gedrückt, bis sich die Orientierung des Teilchens geändert hat.
In \cite{wall} wurde dies allerdings nur für ein Teilchen in einer Flüssigkeit untersucht. 
Hier soll nun überprüft werden, ob die hier verwendeten angetriebenen Teilchen sich ähnlich verhalten.
%Daher wird im Folgenden die räumliche Verteilung der Teilchenzahl $N_c$ in der Zelle $c$ betrachtet.
 
\subsection{Die Implementierungen}
Es wird wieder der MPCD-Algorithmus mit Antrieb für das Rechteck und dessen entsprechenden Parameter verwendet.
Da der zeitliche Verlauf der Teilchenzahlverteilung nicht von Interesse ist, wird hier der Messvorgang nur einmal und nur für die Antriebsstärke von $\alpha = \num{0,01} \, m_i a / \updelta t^2$ und $\lambda = \num{0.01}$ ausgeführt. 
Die Simulation läuft somit über $\num{6000}$ Zeitschritte, von denen über $\num{1000}$ gemittelt die Teilchenzahlverteilung aufgenommen wird.

Zur Vergleichbarkeit wird die Simulation mit zwei Implementierungen ausgeführt. 
Bei einer wird dabei der Kollisionsschritt aus Kapitel \ref{SRD} überall außer den Wandzellen abgeschaltet, sodass zwischen den Teilchen keine Wechselwirkung mehr besteht.
Die SRD-Kollision bleibt an den Wandzellen eingeschaltet, um dissipative Wechselwirkungen von Teilchen mit der Wand zu erlauben.
Bei der anderen Implementierung wird der Algorithmus wie zuvor ausgeführt.

\subsection{Ergebnis für das Rechteck}
Wie in Abbildung \ref{dichte_N} und \ref{dichte-Y} zu sehen, lässt sich bestätigen, dass sich die Teilchenzahl $N_c$ am Rand erhöht.
Bei eingeschaltetem SRD-Kollisionsschritt ist jedoch die Teilchenzahl am Rand weit unterhalb des Ergebnisses, wenn der Kollisionsschritt nur an den Wandzellen ausgeführt wird.
Bei ausgeschaltetem Kollisionsschritt geht $N_c$ in der Nähe der Wände bis über $\num{80}$ Teilchen, während bei eingeschaltetem Kollisionsschritt $N_c$ die $\num{30}$ Teilchen nicht überschreitet.
Somit ist der Effekt bei abgeschalteten Wechselwirkungen unter den Teilchen wesentlich deutlicher zu sehen.

Zusätzlich ist ebenfalls zu beobachten, dass sich in den äußersten Wandzellen wesentlich weniger Teilchen befinden als in denen direkt daneben.
Dies ist nur ein Artefakt, das dadurch zustande kommt, dass die äußersten Wandzellen nur zur Hälfte (bzw. zu einem Viertel) innerhalb des Rechtecks liegen.
Daher ist die Teilchenanzahl dort natürlich auch nur halb (viertel) so groß wie in den Nicht-Wandzellen.
\begin{figure}[h]
\centering
\subfigure[Teilchenzahlverteilung $N_c$ ohne SRD-Wechselwirkung. \label{dichte_N}]
{
\includegraphics[width=\textwidth]{Abbildungen/dichte-N-0.pdf}
}
\hfill
\subfigure[Teilchenzahlverteilung $N_c$ mit SRD-Wechselwirkung. \label{dichte-Y}]
{
\includegraphics[width=\textwidth]{Abbildungen/dichte-Y-0.pdf}
}
\hfill
\caption[$N_c$ mit und ohne SRD-Wechselwirkung bei einem Antrieb von $\alpha = \num{0.01} \, m_i a / \updelta t^2$ und $\lambda = \num{0.01}$ für die rechteckige Geometrie]{Die Teilchenzahlverteilung $N_c$ mit und ohne SRD-Wechselwirkung bei einem Antrieb von $\alpha = \num{0.01}\, m_i a / \updelta t^2$ und $\lambda = 0.01$ für die rechteckige Geometrie, nachdem der Algorithmus $\num{6000}$ Zeitschritte gelaufen ist und über die letzten $\num{1000}$ gemittelt wurde. Die unterschiedlichen Skalen bei beiden Abbildungen sind zu beachten.}
\end{figure}